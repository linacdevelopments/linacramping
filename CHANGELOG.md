# CHANGELOG
## Version 0.0.1
First version of the device.

## Version 0.1.0
First stable verison

## Version 0.2.0
Add KlystronReset feature to device.

## Version 0.3.0
Pending.

## Version 0.4.0
Pending.

## Version 0.5.0
Pending.

## Version 0.5.1
Solve bug of Tango properties and other improvements.
  - Solve bug with properties. Now ky_mode is working as expected.
  - Modify example YAML files. Add testing attributes.
  - Correct README doc.

## Version 0.6.0
Update Setup methods to pyproject.toml