.. LinacRamping documentation master file, created by
   sphinx-quickstart on Fri Feb  9 13:25:50 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Welcome to LinacRamping's documentation!
========================================
LinacRamping is a generic ramping device that will ramp tango attributes with
certains constrains

Introduction
############

Project Overview
****************

The aim of this project is to enhance the capabilities of the ALBA Linac by
introducing the ability to ramp certain Tango attributes. The primary objective
is to reduce operational stress on specific components and perform conditioning
when necessary. This proactive approach aims to prevent vacuum peaks in
acceleration section cavities, arcs in waveguides, and other potential issues.

Device Functionality
********************
The proposed device will systematically ramp attributes from a safe starting
point to the expected working/conditioning point. This process is crucial for
maintaining optimal performance and longevity of the components involved.
In the event of a failure or reaching an alarm point, the ramp will be halted,
and an email notification will be sent to predefined recipients.

Ramp Configuration
******************

Users can define a ramp by specifying the following parameters:

- Attribute to Ramp
- Readback Attribute (if it exists; use _None_ if not applicable)
- Start Point
- End Point
- Delta Setpoint
- Time Between Points

Additionally, each ramp is launched as an independent thread, allowing for the
concurrent execution of multiple ramps. This design ensures flexibility and
efficiency in managing the ramping process.

For more information about how to use the device server, please read README file
at `GitLab proyect <https://gitlab.com/linacdevelopments/linacramping>`_


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
