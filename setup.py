# -*- coding:utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


from setuptools import setup, find_packages

description = "Linac ramping Tango device."
long_description = (
    "Project to enhance the capabilities"
    "of the ALBA Linac by introducing the ability to ram"
    "certain Tango attributes."
)

author = "Emilio Morales"
maintainer = author
maintainer_email = "emorales@cells.es"
__version__ = "0.9.1"
url = "https://gitlab.com/linacdevelopments/linacramping"
download_url = url
copyright = "Copyright 2023, CELLS / ALBA Synchrotron"
platforms = ["Linux"]


install_requires = ["pytango", "pyyaml"]

entry_points = {
    "console_scripts": ["LinacRamping = src.LinacRamping:main"],
}

classifiers = [
    "Development Status :: 3 - Alpha",
    "Intended Audience :: Developers",
    "Intended Audience :: Science/Research",
    (
        "License :: OSI Approved :: "
        + "GNU General Public License v3 or later (GPLv3+)"
    ),
    "Operating System :: POSIX :: Linux",
    "Operating System :: Unix",
    "Operating System :: OS Independent",
    "Natural Language :: English",
    "Programming Language :: Python :: 3.7",
    "Programming Language :: Python :: 3.10",
    "Programming Language :: Python :: 3.11",
    "Topic :: Scientific/Engineering",
]


setup(
    name="LinacRamping",
    version=__version__,
    description=description,
    long_description=long_description,
    author=author,
    maintainer=maintainer,
    maintainer_email=maintainer_email,
    url=url,
    download_url=download_url,
    platforms=platforms,
    license="GPLv3+",
    packages=find_packages(),
    classifiers=classifiers,
    include_package_data=True,
    entry_points=entry_points,
    python_requires=">=3.7",
    install_requires=install_requires,
)
