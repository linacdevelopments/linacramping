# -*- coding:utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Emilio Morales <emorales@cells.es>"
__maintainer__ = "Emilio Morales"
__copyright__ = "Copyright 2023, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


from datetime import datetime, timedelta
import traceback

import tango

DELTA_TIME = timedelta(hours=8, minutes=0)
RESET_COMMAND = "HVPS_Interlock_RC"

# Interlocks that can be reseted:
# Interlock 5 --> arc overcurrent
# Interlcok 6 --> RF reflected power
INTERLOCKS = [5, 6]


class KlystronReset(object):
    """Class to provide an interface to reset Linac klystron automatically."""

    def __init__(self, name, start_time):
        super(KlystronReset, self).__init__()

        self._kyDeviceProxy = None
        self._numReset = 0

        self._kyName = name
        self._startTime = start_time

        self._lastReset = None

        self.kyDeviceProxy = self._kyName

    def _getKyStatus(self):
        """Method to check klystron pulser unit status

        :return: Pulser unit status.
        :rtype: int
        """
        try:
            if self._kyDeviceProxy is not None:
                return self._kyDeviceProxy.read_attribute("Pulse_Status")
            else:
                print("No device proxy crearted.")
        except Exception as e:
            print("Problems in _getKyStatus: {}".format(e))
            print(traceback.format_exc())

    def _checkTime(self):
        """Method to check that between 2 consecutive resets at least we wait
        a DELTA_TIME.

        :return: True if we can perform a reset, False otherwise.
        :rtype: bool
        """
        try:
            if self._lastReset is None:
                return True

            result = (datetime.now() - self.lastReset) - DELTA_TIME
            if result >= timedelta(hours=0, minutes=0):
                return True
            else:
                print(
                    "Time NOT OK. Last reset was at {}.".format(self.lastReset)
                )
                return False
        except Exception as e:
            print("Problems in _checkTime: {}".format(e))
            print(traceback.format_exc())

    def _checkTimes(self):
        """Method to cehck numbers of resets done during the same day.

        :return: True if less than 3 times, False otherwise.
        :rtype: bool
        """
        try:
            if self.numReset < 3:
                return True
            else:
                print("Number of resets NOT OK: {}".format(self.numReset))
                return False
        except Exception as e:
            print("Problems in _checkTimes: {}".format(e))
            print(traceback.format_exc())

    def _checkTypes(self, type):
        """Method that check if the reset type is inside list of permited type
        of interlocks.

        :param type: Type of interlock to check.
        :type type: int
        :return: True if type is allowed to reset, False otherwise.
        :rtype: bool
        """
        try:
            if type is not None and type in INTERLOCKS:
                return True
            else:
                return False
        except Exception as e:
            print("Problems in _checkTypes: {}".format(e))
            print(traceback.format_exc())

    def ResetKY(self, type=None):
        """Method that perform the KY rest.

        :return: True if reset performed, False otherwise.
        :rtype: bool
        """
        try:
            _weCanTime = self._checkTime()
            _weCanTimes = self._checkTimes()
            _weCanTypes = self._checkTypes(type)

            if all([_weCanTime, _weCanTimes, _weCanTypes]) is True:
                self.kyDeviceProxy.write_attribute(RESET_COMMAND, True)
                self.numReset += 1
                self._lastReset = datetime.now()
                return True
            else:
                return False
        except Exception as e:
            print("Problems in ResetKY method: {}".format(e))
            print(traceback.format_exc())
            return False

    @property
    def numReset(self):
        """Class attrribute: Number of resets.

        :return: Number of resets.
        :rtype: int
        """
        return self._numReset

    @numReset.setter
    def numReset(self, num):
        """Number of resets setter.

        :param num: Number of resets.
        :type num: int
        """
        try:
            self._numReset = num

        except Exception as e:
            print("Problem in numReset.setter: {}".format(e))
            print(traceback.format_exc())

    @property
    def kyDeviceProxy(self):
        """Class attribute: DeviceProxy of the Klystron.

        :return: DeviuceProxy object.
        :rtype: tango.DeviceProxy
        """
        return self._kyDeviceProxy

    @kyDeviceProxy.setter
    def kyDeviceProxy(self, name):
        """DeviceProxy setter.

        :param name: Device server name in tango format: 'a/b/c'
        :type name: str
        """
        try:
            if name not in [None]:
                self._kyDeviceProxy = tango.DeviceProxy(name)
            else:
                print("No name provided.")
        except Exception as e:
            print("Problems in kyDeviceProxy.setter:{}".format(e))
            print(traceback.format_exc())

    @property
    def lastReset(self):
        """Class attribute: Last reset.

        :return: Last reset time.
        :rtype: datetime
        """
        return self._lastReset


if __name__ in ["__main__"]:
    import time

    date = datetime.now() - timedelta(hours=9)
    dev = "li/ct/plc4"
    rs = KlystronReset(dev, date)

    print("Device Proxy: {}".format(rs.kyDeviceProxy))
    print("Num. Resets: {}".format(rs.numReset))
    print("Last Reset: {}".format(rs.lastReset))
    print("Try to reset Klystron...")
    try:
        for i in range(5):
            is_ok = rs.ResetKY()
            if is_ok is True:
                print("Reset done. Last Reset: {}".format(rs.lastReset))
            time.sleep(3)
    except Exception as e:
        print("{}\n{}".format(e, traceback.format_exc()))
